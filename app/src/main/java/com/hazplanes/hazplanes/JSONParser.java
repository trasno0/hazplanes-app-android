package com.hazplanes.hazplanes;

import android.text.Html;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class JSONParser {

    public ArrayList<EventoItem> getHazPlanesDataFromJson(String hazplanesJsonStr)
            throws JSONException {

        //JSON Node Names
        final String HP_DATA = "data";
        final String HP_ID = "id";
        final String HP_NOMBRE = "nombre";
        final String HP_POSTER = "poster";
        String HP_FECHA = "fecha";
        String HP_GENERO = "genero";
        final String HP_TIPOEVENTO = "tipoevento";
        final String HP_AGOTADO = "agotado";
        final String HP_CANCELADO = "cancelado";
        final String HP_INFANTIL = "infantil";
        final String HP_RESERVA = "reserva";
        final String HP_TODOELDIA = "todoeldia";
        final String HP_COORD = "coordenadas";
        String tipoevento = "";
        String datetemp = "";

        JSONObject hazplanesJson = new JSONObject(hazplanesJsonStr);
        if( hazplanesJson.length() < 1) {
            return null;
        }
        JSONArray hazplanesArray = hazplanesJson.getJSONArray(HP_DATA);

        ArrayList<EventoItem> results = new ArrayList<EventoItem>(), temp = new ArrayList<EventoItem>();
        ArrayList<String> ids = new ArrayList<String>();
        for(int i = 0; i < hazplanesArray.length(); i++) {
            // Get the JSON object representing the plan
            JSONObject planData = hazplanesArray.getJSONObject(i);

            if(planData.has("idexposicion")) {
                HP_FECHA = "inicio";
            }
            if(ids.contains(planData.getString(HP_NOMBRE)+planData.getString(HP_FECHA).substring(0,10))) {
                continue;
            }
            ids.add(planData.getString(HP_NOMBRE)+planData.getString(HP_FECHA).substring(0,10));
            HP_FECHA = "fecha";

            EventoItem evento = new EventoItem();
            if(planData.has(HP_ID)) {
                evento.setID(planData.getString(HP_ID));
            } else if(planData.has("idpelicula")) {
                tipoevento = "peliculas";
                evento.setID(planData.getString("idpelicula"));
            } else if(planData.has("idconcierto")) {
                tipoevento = "conciertos";
                evento.setID(planData.getString("idconcierto"));
            } else if(planData.has("idobrateatro")) {
                tipoevento = "obrasteatro";
                evento.setID(planData.getString("idobrateatro"));
            } else if(planData.has("iddeporte")) {
                tipoevento = "deportes";
                evento.setID(planData.getString("iddeporte"));
            } else if(planData.has("idexposicion")) {
                tipoevento = "exposiciones";
                evento.setID(planData.getString("idexposicion"));
            } else if(planData.has("idcurso")) {
                tipoevento = "formacion";
                HP_GENERO = "tipo";
                evento.setID(planData.getString("idcurso"));
            } else if(planData.has("ideventos")) {
                tipoevento = "eventos";
                HP_GENERO = "tipo";
                evento.setID(planData.getString("ideventos"));
            }
            evento.setNombre(planData.getString(HP_NOMBRE));
            String genre = planData.getString(HP_GENERO);
            if(!genre.equals("null")) {
                evento.setGenero(genre);
            } else {
                evento.setGenero("Sin determinar");
            }
            if(planData.has(HP_FECHA)) {
                evento.setFecha(planData.getString(HP_FECHA));
            } else {
                Date d = new Date();
                evento.setFecha(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
                //evento.setFecha("");
            }
            if(planData.has(HP_TIPOEVENTO)) {
                evento.setTipoevento(planData.getString(HP_TIPOEVENTO));
            } else {
                evento.setTipoevento(tipoevento);
            }
            if(planData.has(HP_COORD) && !planData.getString(HP_COORD).equals("")) {
                evento.setCoordenadas(planData.getString(HP_COORD));
            }
            evento.setAgotado(planData.getString(HP_AGOTADO).equals("1"));
            evento.setCancelado(planData.getString(HP_CANCELADO).equals("1"));
            evento.setInfantil(planData.getString(HP_INFANTIL).equals("1"));
            if(planData.has(HP_RESERVA)) {
                evento.setReserva(planData.getString(HP_RESERVA).equals("1"));
            }
            if(planData.has(HP_TODOELDIA)) {
                evento.setTodoeldia(planData.getString(HP_TODOELDIA).equals("1"));
            }
            evento.setPosterUrl(planData.getString(HP_POSTER));

            String[] date = evento.getFecha().split(" ");
            if(!date[0].equals(datetemp)) {
                try {
                    EventoItem evento2 = new EventoItem();

                    Date fecha = new SimpleDateFormat("yyyy-MM-dd", new Locale("es_ES")).parse(date[0]);
                    String fechaLarga = new SimpleDateFormat("EEEE, dd 'de' MMMM 'de' yyyy").format(fecha);
                    evento2.setNombre(fechaLarga.substring(0, 1).toUpperCase() + fechaLarga.substring(1));
                    evento2.setGenero("separadorFecha");
                    results.add(evento2);
                    datetemp = date[0];
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            String hora = evento.getFecha().substring(11,13);
            if(Integer.parseInt(hora) < 6 && !evento.getFecha().substring(11).equals("00:00:00")) {
                temp.add(evento);
            } else {
                results.add(evento);
            }
        }
        results.addAll(temp);

        return results;
    }

    public ArrayList<EventoItem> getEventoDataFromJson(String planJsonStr, String tipoevento)
            throws JSONException {

        //JSON Node Names
        final String HP_DATA = "data";
        String HP_NOMBRE = "nombre";
        String HP_POSTER = "poster";
        String HP_GENERO = "genero";
        String HP_SINOPSIS = "sinopsis";
        String HP_NOTAS = "notas";
        String HP_MENCIONES = "menciones";
        String HP_WEB = "web";
        String HP_VIDEO = "video";
        String HP_UPDATED = "actualizado";
        String HP_CREATED = "creado";
        final String HP_INFANTIL = "infantil";
        final String HP_RESERVA = "reserva";
        final String HP_LIMITERESERVA = "limitereserva";

        JSONObject planJson = new JSONObject(planJsonStr);
        JSONArray planArray = planJson.getJSONArray(HP_DATA);
        JSONArray horas;

        ArrayList<EventoItem> result = new ArrayList<EventoItem>();
        ArrayList<JSONArray> sesiones = new ArrayList<JSONArray>();

        //for(int i = 0; i < planArray.length(); i++) {
        if(planArray.length() > 0) {
            // Get the JSON object representing the plan
            JSONObject planData = planArray.getJSONObject(0);

            EventoItem evento = new EventoItem();
            //evento.setID(planData.getString(HP_ID));
            evento.setNombre(planData.getString(HP_NOMBRE));
            evento.setTipoevento(tipoevento);
            if(tipoevento.equals("peliculas")) {
                HP_VIDEO = "trailer";
                evento.setDirector(planData.getString("director"));
                evento.setActores(planData.getString("actores"));
                evento.setNacionalidad(planData.getString("nacionalidad"));
                evento.setDistribuidora(planData.getString("distribuidora"));
                evento.setEdad(planData.getString("edad"));
                evento.setDuracion(planData.getString("duracion"));
                evento.setFechaestreno(planData.getString("fechaestreno"));
            }
            if(tipoevento.equals("eventos")) {
                HP_GENERO = "tipo";
                HP_SINOPSIS = "descripcion";
            }
            if(tipoevento.equals("formacion")) {
                HP_GENERO = "tipo";
                HP_SINOPSIS = "descripcion";
            }
            if(tipoevento.equals("deportes")) {
                HP_GENERO = "tiponombre";
                HP_SINOPSIS = "descripcion";
            }
            if(tipoevento.equals("exposiciones")) {
                HP_SINOPSIS = "descripcion";
            }
            if(planData.has(HP_VIDEO)) {
                evento.setVideo(planData.getString(HP_VIDEO));
            }
            if(!planData.getString(HP_GENERO).equals("null")) {
                evento.setGenero(planData.getString(HP_GENERO));
            } else {
                evento.setGenero("Sin determinar");
            }
            evento.setSinopsis(Html.fromHtml(planData.getString(HP_SINOPSIS)).toString());
            evento.setNotas(Html.fromHtml(planData.getString(HP_NOTAS)).toString());
            evento.setMenciones(Html.fromHtml(planData.getString(HP_MENCIONES)).toString());
            evento.setWeb(planData.getString(HP_WEB));
            evento.setActualizado(planData.getString(HP_UPDATED));
            evento.setCreado(planData.getString(HP_CREATED));
            evento.setInfantil(planData.getString(HP_INFANTIL).equals("1"));
            evento.setReserva(planData.getString(HP_RESERVA).equals("1"));
            evento.setLimitereserva(planData.getString(HP_LIMITERESERVA));
            evento.setPosterUrl(planData.getString(HP_POSTER));

            if(planData.has("horasCine")) {
                horas = planData.getJSONArray("horasCine");
                sesiones.add(horas);
            }
            if(planData.has("horasLocal")) {
                horas = planData.getJSONArray("horasLocal");
                sesiones.add(horas);
            }
            if(planData.has("horasAuditorio")) {
                horas = planData.getJSONArray("horasAuditorio");
                sesiones.add(horas);
            }
            if(planData.has("horasLugar")) {
                horas = planData.getJSONArray("horasLugar");
                sesiones.add(horas);
            }
            if(planData.has("horasMuseo")) {
                horas = planData.getJSONArray("horasMuseo");
                sesiones.add(horas);
            }
            if(planData.has("horasPabellon")) {
                horas = planData.getJSONArray("horasPabellon");
                sesiones.add(horas);
            }

            evento.setSesiones(sesiones);

            result.add(evento);
        }

        return result;
    }
}

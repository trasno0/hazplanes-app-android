package com.hazplanes.hazplanes.data;

import android.provider.BaseColumns;

/**
 * Created by imac on 06/08/14.
 */
public class HazPlanesContract {

    public static final class HazPlanesEntry implements BaseColumns {

        public static final String TABLE_NAME = "planes";

        public static final String COLUMN_DATETEXT = "fecha";

        public static final String COLUMN_PLAN_ID = "plan_id";
    }
}
